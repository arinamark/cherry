(function($){
   var elsize = 150,
       step = 80,
       zIndex = 1,
       NUMBER_OF_CHERRY,
       tEl = 0,
       animated = false,
       animateDelay = 300;

   function randomInteger(low, high){
        return low + Math.floor(Math.random() * (high - low));
   }


    function pixelValue(value){
        return value + 'px';
    }

    function createACherry(){
        var cherryDiv = $('<div></div>'),
            container = $('#cherryContainer'),
            max = container.width() + 150,
            top,
            num;

        if(randomInteger(0, max) < 20){
            num = randomInteger(13, 16);
        }else{
            num  = randomInteger(1, 12);
        };
        cherryDiv.addClass('animated lightspeed bounceInDown cherry-bl');
        cherryDiv.addClass('cherry-bl-' + num);

        top = container.height() - step;
        cherryDiv.css({
            'top': pixelValue(top),
            'left': pixelValue(randomInteger(0, max) - 150),
            'z-index': zIndex
        });

        return cherryDiv;
    }

    function newRow(){
        var el = $('#cherryContainer'),
            bg = el.next(),
            cherries = el.find('.cherry-bl'),
            prev = bg.find('.cherry-bl'),
            max = el.height() - step,
            min = bg.height() + step;

        if(zIndex == 1) min = min - 50;

        cherries.each( function() {
            $(this).removeClass('animated');
            $(this).css({
                'top': 'auto',
                'bottom': pixelValue(min - step - 20)
            });
            bg.append(this);
        });

        zIndex++;

        bg.css({'height': pixelValue(min)});
        el.css({'height': pixelValue(max)});
    };

   function init(){
       var container = $('#cherryContainer'),
           timer;

       NUMBER_OF_CHERRY = parseInt(container.width()/elsize)*10;

       if(tEl < NUMBER_OF_CHERRY){
           container.append(createACherry());
           tEl = tEl + 1;
           timer = setTimeout(function(){
               animated = false;
           },animateDelay);
       }else{
           tEl = 0;
           newRow();
           animated = false;
       };
   };

   $(window).load(function(){
       var newScroll = $('<div></div>'),
           height = $(document).height(),
           numberOfRows = parseInt(height/step);

       newScroll.css({
           'width': '100%',
           'height': numberOfRows*height,
           'position': 'relative',
           'top': 0,
           'left': 0,
           'z-index': 2000
       });

       $('.content').append(newScroll);

       $('.content').scroll(function(){
           if(animated){
               newScroll.css({
                   'height': newScroll.height() + height
               });
              return false;
           }else{
               if($('.cherry-drop').height() > 0){
                   animated = true;
                   newScroll.css({
                       'height': newScroll.height() + height
                   });
                   init();
               }else{
                 newScroll.remove();
               };
           };
       });
   });
})(jQuery);
